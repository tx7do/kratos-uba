package service

import (
	"context"
	"kratos-uba/pkg/util/trans"

	"github.com/go-kratos/kratos/v2/log"
	"google.golang.org/protobuf/types/known/emptypb"

	"kratos-uba/app/core/service/internal/biz"

	"kratos-uba/gen/api/go/common/pagination"
	v1 "kratos-uba/gen/api/go/user/service/v1"
)

type UserService struct {
	v1.UnimplementedUserServiceServer

	uc  *biz.UserUseCase
	log *log.Helper
}

func NewUserService(logger log.Logger, uc *biz.UserUseCase) *UserService {
	l := log.NewHelper(log.With(logger, "module", "user/service/core-service"))
	return &UserService{
		log: l,
		uc:  uc,
	}
}

func (s *UserService) ListUser(ctx context.Context, req *pagination.PagingRequest) (*v1.ListUserResponse, error) {
	resp, err := s.uc.List(ctx, req)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(resp.Items); i++ {
		resp.Items[i].Password = trans.String("")
	}

	return resp, nil
}

func (s *UserService) GetUser(ctx context.Context, req *v1.GetUserRequest) (*v1.User, error) {
	resp, err := s.uc.Get(ctx, req)
	if err != nil {
		return nil, err
	}

	resp.Password = trans.String("")

	return resp, nil
}

func (s *UserService) CreateUser(ctx context.Context, req *v1.CreateUserRequest) (*v1.User, error) {
	resp, err := s.uc.Create(ctx, req)
	if err != nil {
		// s.log.Info(err)
		return nil, err
	}

	resp.Password = trans.String("")

	return resp, nil
}

func (s *UserService) UpdateUser(ctx context.Context, req *v1.UpdateUserRequest) (*v1.User, error) {
	resp, err := s.uc.Update(ctx, req)
	if err != nil {
		// s.log.Info(err)
		return nil, err
	}

	resp.Password = trans.String("")

	return resp, nil
}

func (s *UserService) DeleteUser(ctx context.Context, req *v1.DeleteUserRequest) (*emptypb.Empty, error) {
	_, err := s.uc.Delete(ctx, req)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}
